//
//  FirstViewController.swift
//  Teste9
//
//  Created by Catarina Silva on 20/09/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        labelText.text = "OK, button was pressed!"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

